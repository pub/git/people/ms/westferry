#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import tornado.httpserver
import tornado.ioloop

from . import application

class Service(object):
	pass


class WebService(Service):
	def __init__(self, port=80):
		self.port = port

	def make_application(self, **kwargs):
		return application.WebApplication(**kwargs)

	@property
	def ioloop(self):
		return tornado.ioloop.IOLoop.instance()

	def run(self, **kwargs):
		app = self.make_application(**kwargs)

		# Create a HTTP server instance
		server = tornado.httpserver.HTTPServer(app)
		server.bind(self.port)

		# Launch the server
		server.start()

		# Launch the IOLoop
		self.ioloop.start()
