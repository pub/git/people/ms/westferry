#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import collecty.client

from . import base

class GraphsBackend(base.BaseBackend):
	@property
	def collecty(self):
		"""
			Returns a CollectyClient object which is created
			when it is needed.
		"""
		return collecty.client.Collecty()

	@staticmethod
	def _remove_defaults(kwargs):
		# Remove all keys with value None
		# dbus cannot marshall None and when None is set, we assume
		# that the default is selected, so we can simply remove the
		# setting.
		for key in kwargs.copy():
			if kwargs[key] is None:
				del kwargs[key]

		return kwargs

	def generate_graph(self, template_name, **kwargs):
		assert self.template_exists(template_name)
		kwargs = self._remove_defaults(kwargs)

		return self.collecty.generate_graph(template_name, **kwargs)

	def graph_info(self, template_name, **kwargs):
		assert self.template_exists(template_name)
		kwargs = self._remove_defaults(kwargs)

		return self.collecty.graph_info(template_name, **kwargs)

	def template_exists(self, template_name):
		"""
			Returns True if a template with the given name exists
		"""
		return template_name in self.collecty.list_templates()
