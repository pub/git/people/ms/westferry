#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

from . import base

import logging
log = logging.getLogger(__name__)

class Form(object):
	def __init__(self, handler, id, method="POST", action="",
			form=None, label=None, help=None, disabled=False):
		self.handler = handler
		self.id = id
		self.method = method
		self.action = action
		self.form = form
		self.label = label
		self.help = help

		self.elements = []

		# Is this form disabled?
		self.disabled = disabled

	def __repr__(self):
		return "<%s id=%s>" % (self.__class__.__name__, self.id)

	def __iter__(self):
		"""
			Iterating over a form will iterate over its elements.
		"""
		for element in self.elements:
			yield element

			if isinstance(element, Form):
				for e in element:
					yield e

	def export(self):
		"""
			Returns a dictionary with all keys and values
			of the form.
		"""
		ret = {}

		for element in self:
			# Skip any sub-form elements
			if isinstance(element, Form):
				continue

			ret[element.name] = element.value

		return ret

	def execute(self):
		"""
			Executes the form action
		"""
		log.debug("Executing form %s" % self)

		for element in self:
			log.debug("  Processing element %s" % element)

			# Call the store callback if available
			if element.store and callable(element.store):
				element.store(element.value)

			# Otherwise try setting attribute to object
			elif element.object and element.attribute:
				setattr(element.object, element.attribute, element.value)

			else:
				log.warning("Value of %s could not be stored" % element)

	# Sub-Forms

	def _add_subform(self, cls, *args, **kwargs):
		form = cls(self.handler, self.id, *args, form=self, **kwargs)
		self.elements.append(form)

		return form

	def add_group(self, *args, **kwargs):
		return self._add_subform(Group, *args, **kwargs)

	def add_fieldset(self, *args, **kwargs):
		return self._add_subform(Fieldset, *args, **kwargs)

	# Elements

	def _add_element(self, cls, *args, **kwargs):
		"""
			Adds a new form element to this form.
		"""
		element = cls(self,  *args, **kwargs)
		self.elements.append(element)

		return element

	def get_element_by_name(self, name):
		"""
			Returns the element with the given name (if any).
		"""
		for element in self:
			if element.name == name:
				return element

	# Have a shortcut to find elements by their name quickly.
	__getattr__ = get_element_by_name

	def add_text_input(self, *args, **kwargs):
		"""
			Creates a new text input
		"""
		return self._add_element(TextInput, *args, **kwargs)

	def add_multiline_text_input(self, *args, **kwargs):
		"""
			Creates a new multiline text input
		"""
		return self._add_element(MultilineTextInput, *args, **kwargs)

	def add_password_input(self, *args, **kwargs):
		"""
			Creates a new password input
		"""
		return self._add_element(PasswordInput, *args, **kwargs)

	def add_yesno_input(self, *args, **kwargs):
		"""
			Creates a new yes/no input
		"""
		return self._add_element(YesNoInput, *args, **kwargs)

	def add_single_choice_input(self, *args, **kwargs):
		"""
			Creates a new single choice input
		"""
		return self._add_element(SingleChoiceInput, *args, **kwargs)

	def add_multiple_choice_input(self, *args, **kwargs):
		"""
			Creates a new multiple choice input
		"""
		return self._add_element(MultipleChoiceInput, *args, **kwargs)


class Fieldset(Form):
	pass


class FormFieldsetModule(base.BaseUIModule):
	def render(self, fieldset):
		return self.render_string("modules/forms/fieldset.html", fs=fieldset)


class Group(Form):
	pass


class Element(object):
	# The element type
	type = None

	def __init__(self, form, name=None, label=None, default=None,
			disabled=False, readonly=False, object=None, attribute=None, store=None, **kwargs):
		assert self.type

		self.form = form
		self.name = name
		self.label = label
		self.default = default

		# Is this element editable?
		self.disabled = disabled
		self.readonly = readonly

		# Object & Attribute
		self.object = object
		self.attribute = attribute or name

		# Import default value from object and attribute if none set
		if self.default is None and self.object and self.attribute:
			self.default = getattr(self.object, self.attribute)

		# Callbacks
		self.store = store

		# Help text
		self.help = None

		# Call initializer for inheriting classes
		self.initialize(**kwargs)

	def __repr__(self):
		return "<%s name=\"%s\">" % (self.__class__.__name__, self.name)

	def initialize(self):
		pass

	@property
	def value(self):
		"""
			Returns the value that the user entered for this element.
		"""
		return self.form.handler.get_argument(self.name, self.default)


class SingleChoiceInput(Element):
	type = "single-choice"


class MultipleChoiceInput(Element):
	type = "multiple-choice"


class FormModule(base.BaseUIModule):
	def render(self, form):
		return self.render_string("modules/forms/index.html", form=form)


class FormElementsModule(base.BaseUIModule):
	def render(self, elements):
		return self.render_string("modules/forms/elements.html", elements=elements)


class TextInput(Element):
	type = "text"

	def initialize(self, prefix=None, suffix=None, placeholder=None):
		self.prefix = prefix
		self.suffix = suffix

		self.placeholder = placeholder or self.label


class TextInputModule(base.BaseUIModule):
	def render(self, input):
		assert isinstance(input, TextInput)

		return self.render_string("modules/forms/inputs/text.html", input=input)


class PasswordInput(TextInput):
	type = "password"


class MultilineTextInput(TextInput):
	type = "multiline-text"

	DEFAULT_LINES = 3

	def initialize(self, lines=DEFAULT_LINES, **kwargs):
		super().initialize(**kwargs)

		self.lines = lines



class MultilineTextInputModule(base.BaseUIModule):
	def render(self, input):
		assert isinstance(input, MultilineTextInput)

		return self.render_string("modules/forms/inputs/textarea.html", input=input)


class YesNoInput(Element):
	type = "yesno"

	def initialize(self, description=None):
		self.description = description

	def get_value(self):
		value = Element.get_value(self)

		# This only returns true or false
		if value == "on":
			return True

		return False


class YesNoInputModule(base.BaseUIModule):
	def render(self, input):
		assert isinstance(input, YesNoInput)

		return self.render_string("modules/forms/inputs/checkbox.html", input=input)
