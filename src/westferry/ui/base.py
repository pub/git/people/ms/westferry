#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import tornado.web

_ui_methods = {}
_ui_modules = {}

class UIMethodsRegistration(type):
	def __init__(ui_method, name, bases, dict):
		type.__init__(ui_method, name, bases, dict)

		# The main class from which is inherited is not registered
		# as a plugin
		if name == "BaseUIMethod":
			return

		if ui_method.handle is None:
			raise RuntimeError

		_ui_methods[ui_method.handle] = ui_method()

	@staticmethod
	def get_ui_methods():
		return _ui_methods


class UIModulesRegistration(type):
	def __init__(ui_module, name, bases, dict):
		type.__init__(ui_module, name, bases, dict)

		# The main class from which is inherited is not registered
		# as a plugin
		if name == "BaseUIModule" or name.startswith("_"):
			return

		if name.endswith("Module"):
			name = name[:-6]

		_ui_modules[name] = ui_module

	@staticmethod
	def get_ui_modules():
		return _ui_modules


class BaseUIMethod(object, metaclass=UIMethodsRegistration):
	handle = None

	@property
	def backend(self):
		"""
			Shortcut to access the backend
		"""
		return self.handler.backend

	def __call__(self, handler, *args, **kwargs):
		self.handler = handler

		return self.call(*args, **kwargs)

	def call(self, *args, **kwargs):
		raise NotImplementedError


class BaseUIModule(tornado.web.UIModule, metaclass=UIModulesRegistration):
	pass
