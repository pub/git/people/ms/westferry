#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import urllib.parse

from . import base

DEFAULT_INTERVAL = "day"
DEFAULT_FORMAT   = "svg"


class Graph(object):
	def __init__(self, handler, template, object_id=None, title=None):
		self.handler = handler
		self._title = title

		# Graph identifier
		self.template = template
		self.object_id = object_id

		# Set the default interval
		self.interval = handler.get_argument("interval", DEFAULT_INTERVAL)
		self.format = handler.get_argument("format", DEFAULT_FORMAT)

	@property
	def backend(self):
		"""
			Shortcut to the backend.
		"""
		return self.handler.backend

	@property
	def locale(self):
		"""
			Shortcut to the locale
		"""
		return self.handler.locale

	@property
	def graph_info(self):
		if not hasattr(self, "_graph_info"):
			self._graph_info =  self.backend.graphs.graph_info(self.template,
				object_id=self.object_id)

		return self._graph_info

	@property
	def title(self):
		return self._title or self.graph_info.get("title")

	# Format

	def get_format(self):
		return self._format

	def set_format(self, format):
		# TODO check for valid inputs
		self._format = format

	# Interval

	def get_interval(self):
		return self._interval

	def set_interval(self, interval):
		# TODO check for valid inputs
		self._interval = interval

	interval = property(get_interval, set_interval)

	def make_namespace(self, **kwargs):
		ns = {
			"format"   : self.format,
			"interval" : self.interval,
		}
		ns.update(kwargs)

		return ns

	def _make_url(self, url, format=None, **kwargs):
		ns = self.make_namespace(**kwargs)

		url = url % {
			"format"    : format or self.format or DEFAULT_FORMAT,
			"object_id" : self.object_id,
			"template"  : self.template,
		}

		query_string = urllib.parse.urlencode(ns)
		if query_string:
			url += "?%s" % query_string

		return url

	def make_image_url(self, **kwargs):
		if self.object_id:
			url = "/graph/%(template)s/%(object_id)s.%(format)s"
		else:
			url = "/graph/%(template)s.%(format)s"

		return self._make_url(url, **kwargs)

	def make_thumbnail_url(self, **kwargs):
		if self.object_id:
			url = "/graph/thumbnail/%(template)s/%(object_id)s.%(format)s"
		else:
			url = "/graph/thumbnail/%(template)s.%(format)s"

		return self._make_url(url, **kwargs)

	def make_url(self, **kwargs):
		if self.object_id:
			url = "/graph/%(template)s/%(object_id)s"
		else:
			url = "/graph/%(template)s"

		return self._make_url(url, **kwargs)

	def generate_graph(self, **kwargs):
		return self.backend.graphs.generate_graph(self.template,
			object_id=self.object_id, **kwargs)


class GraphBoxModule(base.BaseUIModule):
	def render(self, graph, show_title=True):
		return self.render_string("modules/graphs/box.html", graph=graph,
			show_title=show_title)
