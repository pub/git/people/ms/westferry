#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

from . import base

class Element(object):
	def __init__(self, menu, **kwargs):
		self.menu = menu

		# Call initializer for inheriting classes
		self.initialize(**kwargs)

	def initialize(self):
		pass

	def is_active(self):
		return False


class Header(Element):
	def initialize(self, title):
		self.title = title


class Link(Element):
	def initialize(self, title, handler=None, url=None):
		self.title = title
		self.handler = handler
		self.url = url

	def is_active(self):
		if not self.handler:
			return False

		return isinstance(self.menu.handler, self.handler)


class Menu(object):
	"""
		The main menu object that creates a little menu on the side
	"""
	type = "menu"

	def __init__(self, handler, title=None, parent=None):
		self.handler = handler
		self.title = title
		self.parent = parent

		self.elements = []

	@property
	def locale(self):
		"""
			Shortcut to access the user's locale
		"""
		return self.handler.locale

	def __iter__(self):
		"""
			Iterator over all item in this menu
		"""
		return iter(self.elements)

	def is_active(self):
		return False

	def _add_element(self, cls, *args, **kwargs):
		element = cls(self, *args, **kwargs)
		self.elements.append(element)

		return element

	def add_link(self, *args, **kwargs):
		"""
			Adds a link to the menu
		"""
		return self._add_element(Link, *args, **kwargs)

	def add_handler(self, handler, title=None, **kwargs):
		"""
			Automatically adds a handler to the menu
		"""
		title = title or self.locale.translate(handler.title)

		return self.add_link(title=title, handler=handler,
			url=handler.url, **kwargs)

	def add_header(self, *args, **kwargs):
		"""
			Adds a headline in the menu
		"""
		return self._add_element(Header, *args, **kwargs)

	def add_menu(self, title, **kwargs):
		"""
			Adds a sub-menu to the menu
		"""
		return self._add_element(Menu, title=title, parent=self, **kwargs)


class TopbarMenuModule(base.BaseUIModule):
	def render(self, menu):
		return self.render_string("modules/menus/topbar.html", menu=menu)


class SidebarMenuModule(base.BaseUIModule):
	def render(self, menu, nested=False):
		return self.render_string(
			"modules/menus/sidebar.html",
			menu=menu,
			nested=nested,
		)


class MenuLinkModule(base.BaseUIModule):
	def render(self, element):
		return self.render_string("modules/menus/link.html", element=element)


class MenuDropdownModule(base.BaseUIModule):
	def render(self, menu, nested=False):
		return self.render_string("modules/menus/dropdown.html",
			menu=menu, nested=nested)
