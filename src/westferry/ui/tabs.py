#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2021 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import uuid

from . import base
from . import forms
from . import graphs

class TabsModule(base.BaseUIModule):
	def render(self, tabs):
		return self.render_string("modules/tabs.html", tabs=tabs)


class Tabs(object):
	def __init__(self, handler, id=None):
		self.handler = handler

		# Store ID or generate a random one
		self.id = id or uuid.uuid4()

		self.tabs = {}

	def __getattr__(self, key):
		try:
			return self.tabs[key]
		except KeyError as e:
			raise AttributeError(key) from e

	def __iter__(self):
		for id in self.tabs:
			yield self.tabs[id]

	def add_tab(self, id, *args, **kwargs):
		# Check if a tab with this ID already exists
		#if id in self:
		#	raise ValueError("Tab with ID '%s' already exists" % id)

		# Create a new tab
		self.tabs[id] = tab = Tab(self.handler, id, *args, **kwargs)

		# Return the new tab
		return tab

	def get_form(self, id):
		"""
			Returns the form with a matching ID
		"""
		for tab in self:
			for item in tab.items:
				if isinstance(item, forms.Form):
					if item.id == id:
						return item


class Tab(object):
	def __init__(self, handler, id, title):
		self.handler = handler

		# TODO Check format of ID
		self.id = id

		self.title = title

		# List to store all items that have been added to this tab
		self.items = []

	def _add_item(self, cls, *args, **kwargs):
		item = cls(self.handler, *args, **kwargs)
		self.items.append(item)

		return item

	def add_form(self, id=None, *args, **kwargs):
		if id is None:
			id = self.id

		return self._add_item(forms.Form, id, *args, **kwargs)

	def add_graph(self, *args, **kwargs):
		return self._add_item(graphs.Graph, *args, **kwargs)
