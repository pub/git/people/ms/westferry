#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import tornado.web

from . import backend
from . import handlers
from . import ui

from .constants import *

class WebApplication(tornado.web.Application):
	def __init__(self, **kwargs):
		settings = {
			# Enable compressed output
			"compress_response" : True,

			# Serve static files from our webroot
			"static_path" : WEBROOTDIR,

			# Templates
			"template_path" : TEMPLATESDIR,

			# Use Cross-Site-Request-Forgery protection
			"xsrf_cookies" : True,
		}
		settings.update(kwargs)

		# Get handlers
		h = [(h.url, h) for h in handlers.get_handlers()]

		# Get UI modules and methods
		settings.update({
			"ui_methods" : ui.get_ui_methods(),
			"ui_modules" : ui.get_ui_modules(),
		})

		# Initialise the web application
		tornado.web.Application.__init__(self, handlers=h, **settings)

	@property
	def backend(self):
		if not hasattr(self, "_backend"):
			self._backend = backend.Backend()

		return self._backend
