#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import functools
import tornado.web

from .. import ui
from ..i18n import _, N_

_handlers = []

class HandlerRegistration(type):
	def __init__(handler, name, bases, dict):
		type.__init__(handler, name, bases, dict)

		# The main class from which is inherited is not registered
		# as a plugin
		if name.endswith("BaseHandler"):
			return

		if handler.url is None:
			raise RuntimeError(_("Handler %s is improperly configured") % handler)

		_handlers.append(handler)

	@staticmethod
	def get_handlers():
		return _handlers


class BaseHandler(tornado.web.RequestHandler, metaclass=HandlerRegistration):
	url = None

	title = N_("No Title")

	# Points to the menu map of the section in which the handler is in
	menu = None

	@property
	def backend(self):
		"""
			Shortcut to access the backend
		"""
		return self.application.backend

	@property
	def topmenu(self):
		"""
			Creates the default menu in to the top navigation
		"""
		_ = self.locale.translate

		# XXX This is ugly, but since this file declares the base handler,
		# recursive imports fail
		from . import analytics
		from . import demo
		from . import system

		menu = ui.menus.Menu(self)

		# Analytics
		menu.add_handler(analytics.OverviewHandler, title=_("Analytics"))

		# System
		menu.add_handler(system.SummaryHandler, title=_("System"))

		# Demo (only in debug mode)
		if self.backend.debug:
			submenu = menu.add_menu(_("Demo"))

			submenu.add_handler(demo.OverviewHandler)
			submenu.add_handler(demo.FormsHandler)

		return menu

	@functools.cached_property
	def tabs(self):
		return ui.tabs.Tabs(self)

	def get_template_namespace(self):
		ns = tornado.web.RequestHandler.get_template_namespace(self)

		# Add some global constants
		ns.update({
			"VERSION" : self.backend.version,
		})

		# Add some more
		ns.update({
			"menu" : self.menu,
		})

		return ns

	def get_argument_int(self, name, *args, **kwargs):
		val = self.get_argument(name, *args, **kwargs)

		if val is None:
			return

		try:
			return int(val)
		except ValueError:
			raise tornado.web.HTTPError(400,
				_("Invalid type for '%s', expected integer") % name)

	def get(self):
		# Render the default view
		self.render("default.html")

	def post(self):
		"""
			This is the default handler which will find the correct form
			and execute it.
		"""
		form_id = self.get_argument("form")

		# Find the form
		form = self.tabs.get_form(form_id)
		if not form:
			raise tornado.web.HTTPError(400, "Could not find form with ID '%s'" % form_id)

		# Execute the form action
		form.execute()
