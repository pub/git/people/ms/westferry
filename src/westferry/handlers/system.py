#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2021 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

from . import base
from .. import ui
from ..i18n import N_

class BaseHandler(base.BaseHandler):
	@property
	def menu(self):
		_ = self.locale.translate

		menu = ui.menus.Menu(self, _("System"))

		# Summary
		menu.add_handler(SummaryHandler)

		# Settings
		menu.add_handler(SettingsHandler)

		return menu


class SummaryHandler(BaseHandler):
	title = N_("Summary")
	url = r"/system"




class SettingsHandler(BaseHandler):
	title = N_("Settings")
	url = r"/system/settings"

	def initialize(self):
		# Create a tab with general settings
		self._make_general()

	def _make_general(self):
		_ = self.locale.translate

		tab = self.tabs.add_tab("general", _("General"))

		# Add a form
		form = tab.add_form()

		form.add_text_input("hostname", _("Hostname"), object=self.backend.system)
