#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

import datetime
import mimetypes
import tornado.web

from . import base
from .. import ui

from ..i18n import N_

class BaseHandler(base.BaseHandler):
	@property
	def menu(self):
		_ = self.locale.translate

		m = ui.menus.Menu(self, _("Analytics"))

		# Overview
		m.add_handler(OverviewHandler)

		# Network
		m.add_handler(NetworkOverviewHandler)

		# System
		m.add_handler(SystemOverviewHandler)

		return m

	def render_graphs(self, graphs):
		self.render("graphs.html", graphs=graphs)


class OverviewHandler(BaseHandler):
	url = r"/analytics"
	title = N_("Overview")

	def get(self):
		self.render("base.html")


class NetworkBaseHandler(BaseHandler):
	@property
	def menu(self):
		_ = self.locale.translate

		m = ui.menus.Menu(self, _("Network"))

		# Back...
		m.add_handler(OverviewHandler, title=_("Back..."))

		# Overview
		m.add_handler(NetworkOverviewHandler, title=_("Overview"))

		# Connections
		m.add_handler(NetworkConnectionsHandler)

		# Fragmentation
		m.add_handler(NetworkIPFragmentationHandler)

		return m


class NetworkOverviewHandler(NetworkBaseHandler):
	url = r"/analytics/network"
	title = N_("Network")

	def get(self):
		self.render("base.html")


class NetworkConnectionsHandler(NetworkBaseHandler):
	url = r"/analytics/connections"
	title = N_("Connections")

	def get(self):
		_ = self.locale.translate

		graphs = [
			ui.graphs.Graph(self, "conntrack"),
		]

		self.render_graphs(graphs)


class NetworkIPFragmentationHandler(NetworkBaseHandler):
	url = r"/analytics/network/fragmentation"
	title = N_("Fragmentation")

	def initialize(self):
		_ = self.locale.translate

		for proto, title in (("ipv6", _("IPv6")), ("ipv4", _("IPv4"))):
			tab = self.tabs.add_tab(proto, title=title)

			tab.add_graph("%s-fragmentation" % proto)


class SystemBaseHandler(BaseHandler):
	@property
	def menu(self):
		_ = self.locale.translate

		m = ui.menus.Menu(self, _("System"))

		# Back...
		m.add_handler(OverviewHandler, title=_("Back..."))

		# Overview
		m.add_handler(SystemOverviewHandler, title=_("Overview"))

		# Most interesting items
		m.add_handler(SystemProcessorsHandler)
		m.add_handler(SystemMemoryHandler)
		m.add_handler(SystemTemperaturesHandler)

		# Others
		s = m.add_menu(_("More"))
		s.add_handler(SystemContextSwitchesHandler)
		s.add_handler(SystemInterruptsHandler)

		return m


class SystemOverviewHandler(SystemBaseHandler):
	url = r"/analytics/system"
	title = N_("System")

	def initialize(self):
		_ = self.locale.translate

		tab = self.tabs.add_tab("loadavg", _("Load Average"))
		tab.add_graph("loadavg")


class SystemProcessorsHandler(SystemBaseHandler):
	url = r"/analytics/system/processors"
	title = N_("Processors")

	def initialize(self):
		_ = self.locale.translate

		tab = self.tabs.add_tab("usage", _("Usage"))
		tab.add_graph("processor")

		tab = self.tabs.add_tab("frequency", _("Frequency"))
		tab.add_graph("cpufreq")

		tab = self.tabs.add_tab("temperature", _("Temperature"))
		tab.add_graph("processor-temperature")


class SystemMemoryHandler(SystemBaseHandler):
	url = r"/analytics/system/memory"
	title = N_("Memory")

	def initialize(self):
		_ = self.locale.translate

		tab = self.tabs.add_tab("usage", _("Usage"))
		tab.add_graph("memory")


class SystemTemperaturesHandler(SystemBaseHandler):
	url = r"/analytics/system/temperatures"
	title = N_("Temperatures")

	def get(self):
		_ = self.locale.translate

		graphs = [
			ui.graphs.Graph(self, "sensors-temperature"),
			ui.graphs.Graph(self, "processor-temperature"),
		]

		self.render_graphs(graphs)


class SystemContextSwitchesHandler(SystemBaseHandler):
	url = r"/analytics/system/context-switches"
	title = N_("Context Switches")

	def get(self):
		_ = self.locale.translate

		graphs = [
			ui.graphs.Graph(self, "context-switches"),
		]

		self.render_graphs(graphs)


class SystemInterruptsHandler(SystemBaseHandler):
	url = r"/analytics/system/interrupts"
	title = N_("Interrupts")

	def get(self):
		_ = self.locale.translate

		graphs = [
			ui.graphs.Graph(self, "interrupts"),
		]

		self.render_graphs(graphs)


class GraphExportHandler(base.BaseHandler):
	VALID_INTERVALS = ("hour", "day", "month", "week", "year")
	DEFAULT_INTERVAL = "day"

	SUPPORTED_FORMATS = ("pdf", "png", "svg")

	url = r"/graph(/thumbnail)?/([\w\-]+)(?:/([\w\d\.]+))?\.(%s)" % "|".join(SUPPORTED_FORMATS)

	def get(self, thumbnail, template_name, object_id, format):
		_ = self.locale.translate

		# Get the requested dimensions of the image
		height = self.get_argument_int("height", None)
		width  = self.get_argument_int("width", None)

		# Get the requested interval
		interval = self.get_argument("interval", self.DEFAULT_INTERVAL)
		if interval and not interval in self.VALID_INTERVALS:
			raise tornado.web.HTTPError(400, _("Invalid interval: %s") % interval)

		# Create the graph object
		g = ui.graphs.Graph(self, template_name, object_id=object_id)

		# Generate the graph image
		kwargs = {
			"format"     : format.upper(),
			"interval"   : interval,
			"height"     : height,
			"width"      : width,
			"thumbnail"  : bool(thumbnail),

			# Include the title in the PDF exports
			"with_title" : format == "pdf",
		}
		image = g.generate_graph(**kwargs)

		# Set the HTTP headers
		self._make_headers(format, template_name, object_id)

		# Deliver the content
		self.finish(image.get("image"))

	def _make_headers(self, extension, template_name, object_id):
		# Determine the mime type
		mimetype = mimetypes.types_map.get(".%s" % extension, "application/octet-stream")
		self.set_header("Content-Type", mimetype)

		# Add the timestamp when this graph was generated
		now = datetime.datetime.now()

		# Put together the filename (for downloads)
		filename = [self.backend.system.hostname, template_name, object_id, now.isoformat()]
		filename = "%s.%s" % ("-".join((e for e in filename if e)), extension)

		if extension == "pdf":
			self.set_header("Content-Disposition", "attachment; filename=%s" % filename)
		else:
			self.set_header("Content-Disposition", "inline; filename=%s" % filename)


class GraphHandler(base.BaseHandler):
	url = r"/graph/([\w\-]+)(?:/([\w\d\.]+))?"

	def get(self, template, object_id):
		graph = ui.graphs.Graph(self, template, object_id=object_id)

		self.render("graph.html", graph=graph)
