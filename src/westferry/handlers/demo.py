#!/usr/bin/python3
###############################################################################
#                                                                             #
# Westferry - The IPFire web user interface                                   #
# Copyright (C) 2015 IPFire development team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
###############################################################################

from . import base
from .. import ui

from ..i18n import N_, _

class BaseHandler(base.BaseHandler):
	@property
	def menu(self):
		_ = self.locale.translate

		m = ui.menus.Menu(self, _("Demo"))

		# Overview
		m.add_handler(OverviewHandler)

		# Forms
		m.add_handler(FormsHandler)

		return m


class OverviewHandler(BaseHandler):
	url = r"/demo"
	title = N_("Overview")

	def get(self):
		self.render("demo/index.html")


class FormsHandler(BaseHandler):
	url = r"/demo/forms"
	title = N_("Forms")

	def initialize(self):
		_ = self.locale.translate

		# Create a new tab
		tab = self.tabs.add_tab("example-1", _("Example 1"))

		# Add a form to the tab
		form = tab.add_form("example-1")
		form.submit_text = _("Order")

		# First name
		e = form.add_text_input("first_name", label=_("First Name"), placeholder="John")
		e.help = _("Please enter your first name")

		# Last name
		e = form.add_text_input("last_name", label=_("Last Name"), placeholder="Doe")
		e.help = _("Please enter your last name")

		# We create a fieldset for the address
		fs1 = form.add_fieldset(label=_("Address"))
		fs1.help = _("Please enter your address")

		e = fs1.add_text_input("street", label=_("Street"))
		e = fs1.add_text_input("city", label=_("City"))
		e = fs1.add_text_input("post_code", label=_("Postcode"))

		# Fieldset without label
		fs2 = form.add_fieldset()

		# Donation amount
		e = fs2.add_text_input("donation", label=_("Donation"),
			placeholder=_("Donation"), suffix="€")

		# Notes
		e = fs2.add_multiline_text_input("notes", label=_("Notes"))

		# Subscribe to newsletter?
		e = fs2.add_yesno_input("subscribe_to_newsletter", description=_("Subscribe to newsletter?"))
